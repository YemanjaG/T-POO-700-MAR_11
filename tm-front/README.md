# React + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh

---
 When You open project for the first time, run <code>yarn</code> command to install packages from package.json.
 
Then run <code>yarn dev</code> to run server. Server will automatically run on http://localhost:5173/
.

Please only use YARN package manager to install new packages with command <code>yarn add my-super-package</code>.