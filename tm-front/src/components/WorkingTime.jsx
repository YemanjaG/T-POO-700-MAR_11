import React, {useContext, useEffect, useState} from 'react';
import '../styles/index.css';
import '../styles/WorkingTime.css'

import {UserContext} from "../context/UserContext.jsx";
import {WorkingTimeContext} from "../context/WorkingTimesConsumer.jsx";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import ModalCreate from "./modal/modalCreate.jsx";
import ModalDelete from "./modal/ModalDelete.jsx";
import ModalUpdate from "./modal/ModalUpdate.jsx";

const WorkingTime = () => {

    const {userData, setUserData} = useContext(UserContext);
    const [userActuelle, setUserActuelle] = useState();
    const {workingTimeData, setworkingTimeData } = useContext(WorkingTimeContext)
    const [allWorking, setAllWorking] = useState([])

    useEffect(() => {
        const requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        fetch(`http://localhost:4000/api/workingtimes?user=${userData.id}`, requestOptions)
            .then(response => response.json())
            .then(result =>
            {
                setAllWorking(result.data);
                setUserActuelle(userData);
            })
            .catch(error => console.log('error', error));

    }, [userData, workingTimeData]);

    return (
        <div className="div2">
            <div className="userName">
            UserSelected : {userData.username}
            </div>
            <TableContainer>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell align="center">Start</TableCell>
                            <TableCell align="center">End</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody className="rowUser" >
                        {allWorking.map((row) => (

                            <TableRow
                                key={row.id}
                                onClick={() => setworkingTimeData(row)}
                                className={workingTimeData.id === row.id? 'selectedRow' : ''}
                            >
                                <TableCell component="th" scope="row">
                                {row.id}
                            </TableCell><TableCell component="th" scope="row">
                                {row.start}
                            </TableCell>
                                <TableCell align="right">{row.end}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>






        </div>
    );
};

export default WorkingTime;