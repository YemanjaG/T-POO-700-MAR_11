import React, {useEffect, useState} from 'react';
import "../styles/User.css";
import {useContext} from "react";
import {UserConsumer, UserContext} from "../context/UserContext.jsx";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import DeleteIcon from '@mui/icons-material/Delete';
import ModeEditIcon from '@mui/icons-material/ModeEdit';
import ModalDelete from "./modal/ModalDelete.jsx";
import ModalUpdate from "./modal/ModalUpdate.jsx";
import ModalCreate from "./modal/modalCreate.jsx";
import {WorkingTimeContext} from "../context/WorkingTimesConsumer.jsx";
const User = () => {

    const {userData, setUserData} = useContext(UserContext);
    const {workingTimeData, setworkingTimeData } = useContext(WorkingTimeContext)

    const [allUser, setAllUser] = useState([]);


    useEffect(() => {
        fetch("http://localhost:4000/api/users")
            .then(res => res.json())
            .then(result =>{
                setAllUser(result.data)
            })
    }, [allUser.length, userData.length]);

    const selectRow = (val) =>{
        setUserData(val);
        setworkingTimeData("");
    }

    return (
        <>
        <div className="div1">
            <Paper sx={{ width: '100%', overflow: 'hidden' }}>
            <TableContainer>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            <TableCell>
                                <ModalCreate/>
                            </TableCell>
                            <TableCell>ID</TableCell>
                            <TableCell align="right">Username</TableCell>
                            <TableCell align="right">Email</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody className="rowUser" >
                        {allUser.map((row) => (

                            <TableRow
                                key={row.id}
                                onClick={() => selectRow(row)}
                                className={userData.id === row.id ? 'selectedRow' : ''}
                            >
                                <TableCell component="th" scope="row">
                                    <ModalDelete/>
                                    <ModalUpdate/>
                                </TableCell><TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell><TableCell component="th" scope="row">
                            {row.username}
                                </TableCell>
                                <TableCell align="right">{row.email}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            </Paper>
        </div>
        </>
    );
};

export default User;