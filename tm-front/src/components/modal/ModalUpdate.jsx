import React, {useState} from 'react';
import "../../styles/modal/modalDelete.css";
import ModeEditIcon from '@mui/icons-material/ModeEdit';
import {useContext} from "react";
import {UserContext} from "../../context/UserContext.jsx";

import {
    Box,
    Button,
    Typography,
    Modal,
    TextField
} from '@mui/material';
const ModalUpdate = () =>{

    const {userData, setUserData} = useContext(UserContext);
    const [allUser, setAllUser] = useState([]);
    const [modal, setModal] = useState(false);

    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };

    const updateUser = async () => {
        const user ={
            username : username,
            email : email,
        }
        console.log(user)
        try {
            const response = await fetch(`http://localhost:4000/api/users/${userData.id}`,
                {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
            body : JSON.stringify({user})
            });
            if (response.ok) {
                console.log('User updated successfully');
                setUserData("");
                setModal(false);
            } else {
                console.error('Error updated user:', response.status);
            }
        } catch (error) {
            console.error('Error updated user:', error);
        }
    };

    return (
        <>
            <ModeEditIcon onClick={() => setModal(true)}/>
            <Modal
                open={modal}
                //onClose={modal}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Modifie l'utilisateur
                    </Typography>
                    <div className="inputDiv">
                        <TextField id="outlined-basic" label={userData.id} variant="outlined"/>
                        <TextField id="filled-basic" label={userData.username} variant="filled" onChange={(e) =>  setUsername(e.target.value)}/>
                        <TextField id="standard-basic" type="email" label={userData.email} variant="standard" onChange={(e) => setEmail(e.target.value)}/>
                    </div>
                    <div className="divButton">
                        <Button variant="outlined" color="error" onClick={() => setModal(false)}>
                            Annuler
                        </Button>
                        <Button variant="contained" color="success" onClick={() => updateUser()}>
                            Confirmer
                        </Button>
                    </div>
                </Box>
            </Modal>
        </>

    );

}

export default ModalUpdate;