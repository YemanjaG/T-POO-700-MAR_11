import React, {useContext, useState} from 'react';
import "../../styles/modal/modalCreate.css";

import {
    Box,
    Button,
    Typography,
    Modal,
    TextField
} from '@mui/material';
import {UserContext} from "../../context/UserContext.jsx";

const ModalCreate = () => {

    const [modal, setModal] = useState(false);
    const {userData, setUserData} = useContext(UserContext);
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };

    const addUser = async () => {
        const user = {
            username: username,
            email: email,
        }
        try {
            const response = await fetch(`http://localhost:4000/api/users/`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({user})
                });
            if (response.ok) {
                console.log('User add successfully');
                setUserData("");
                setModal(false);
            } else {
                console.error('Error add user:', response.status);
            }
        } catch (error) {
            console.error('Error add user:', error);
        }
    };

    return (
        <>
            <div className="divAddUser">
                <Button variant="contained" onClick={() => setModal(true)}>Add
                    user</Button>
            </div>
            <Modal
                open={modal}
                //onClose={modal}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6"
                                component="h2">
                        Ajouter Utilisateur
                    </Typography>
                    <div className="inputDiv">
                        <TextField id="filled-basic" label="Username"
                                   variant="filled"
                                   onChange={(e) => setUsername(
                                       e.target.value)}/>
                        <TextField id="standard-basic" type="email"
                                   label="email" variant="standard"
                                   onChange={(e) => setEmail(e.target.value)}/>
                    </div>
                    <div className="divButton">
                        <Button variant="outlined" color="error"
                                onClick={() => setModal(false)}>
                            Annuler
                        </Button>
                        <Button variant="contained" color="success"
                                onClick={() => addUser()}>
                            Confirmer
                        </Button>
                    </div>
                </Box>
            </Modal>
        </>

    );

}

export default ModalCreate;