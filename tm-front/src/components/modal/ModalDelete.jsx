import React, {useEffect, useState} from 'react';
import "../../styles/modal/modalDelete.css";
import DeleteIcon from '@mui/icons-material/Delete';
import ModeEditIcon from '@mui/icons-material/ModeEdit';
import {useContext} from "react";
import {UserContext} from "../../context/UserContext.jsx";

import {
    Box,
    Button,
    Typography,
    Modal,
} from '@mui/material';
const ModalDelete = () =>{
    const {userData, setUserData} = useContext(UserContext);
    const [allUser, setAllUser] = useState([]);
    const [modal, setModal] = useState(false);

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };

    const deleteUser = async () => {
        try {
            const response = await fetch(`http://localhost:4000/api/users/${userData.id}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
            });
            if (response.ok) {
                console.log('User deleted successfully');
                setUserData("");
                setModal(false);
            } else {
                console.error('Error deleting user:', response.status);
            }
        } catch (error) {
            console.error('Error deleting user:', error);
        }
    };

    return (
        <>
            <DeleteIcon onClick={() => setModal(true)}/>
            <Modal
                open={modal}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                    Veux-tu supprimer l'utilisateur {userData.username} ?
                    </Typography>
                    <div className="divButton">
                        <Button variant="outlined" color="error" onClick={() => setModal(false)}>
                            Non
                        </Button>
                        <Button variant="contained" color="success" onClick={() => deleteUser()}>
                            Oui
                        </Button>
                    </div>
                </Box>
            </Modal>
        </>

    );

}

export default ModalDelete;