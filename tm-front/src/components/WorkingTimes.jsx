import React, {useState, useEffect, useContext} from 'react';
import {UserContext} from "../context/UserContext.jsx";
import Button from '@mui/material/Button';
import dayjs from 'dayjs';
import SingleWorkingTime from "./SingleWorkingTime.jsx";
import '../styles/index.css';
import '../styles/WorkingTimes.css';

import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import {WorkingTimeContext} from "../context/WorkingTimesConsumer.jsx";

const WorkingTimes = () => {

    const {userData, setUserData} = useContext(UserContext);
    const {workingTimeData, setworkingTimeData } = useContext(WorkingTimeContext)

    const [workingTime, setWorkingTime] = useState(null);

    const [selectedCreateStartDate, setSelectedCreateStartDate] = useState(new Date());
    const [selectedCreateStartTime, setSelectedCreateStartTime] = useState('');

    const [selectedCreateEndDate, setSelectedCreateEndDate] = useState(new Date());
    const [selectedCreateEndTime, setSelectedCreateEndTime] = useState('');

    const [selectedEditStartDate, setSelectedEditStartDate] = useState(new Date());
    const [selectedEditStartTime, setSelectedEditStartTime] = useState('');

    const [selectedEditEndDate, setSelectedEditEndDate] = useState(new Date());
    const [selectedEditEndTime, setSelectedEditEndTime] = useState('');


    useEffect(() => {
        fetchData().then(r => null)
    }, [userData, workingTimeData]);

    const fetchData = async () => {
        try {
            const response = await fetch(`http://localhost:4000/api/workingtimes/${userData.id}/${workingTimeData.id}`);

            if (!response.ok) {
                throw new Error('Network response was not ok');
            }

            const data = await response.json();
            setWorkingTime(data.data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    const handleCreateWorkingTime = async () => {
        const dayjsStartDate = dayjs(selectedCreateStartDate);
        const dayjsEndDate = dayjs(selectedCreateEndDate);

        const working_time = {
            start: `${dayjsStartDate.format('YYYY-MM-DD')}T${selectedCreateStartTime}:00`,
            end: `${dayjsEndDate.format('YYYY-MM-DD')}T${selectedCreateEndTime}:00`,
        };

        await createWorkingTime(working_time)

        // Réinitialisation des dates et heures sélectionnées
        setworkingTimeData("");
        setSelectedCreateStartDate(new Date());
        setSelectedCreateStartTime('');
        setSelectedCreateEndDate(new Date());
        setSelectedCreateEndTime('');
        setworkingTimeData("");
    };

    const handleEditWorkingTime = async () => {
        const dayjsStartDate = dayjs(selectedEditStartDate);
        const dayjsEndDate = dayjs(selectedEditEndDate);

        const working_time = {
            start: `${dayjsStartDate.format('YYYY-MM-DD')}T${selectedEditStartTime}:00`,
            end: `${dayjsEndDate.format('YYYY-MM-DD')}T${selectedEditEndTime}:00`,
        };

        await updateWorkingTime(working_time)

        // Réinitialisation des dates et heures sélectionnées
        setSelectedEditStartDate(new Date());
        setSelectedEditStartTime('');
        setSelectedEditEndDate(new Date());
        setSelectedEditEndTime('');
    }
    const createWorkingTime = async (working_time) => {
        try {
            // Effectue la requête API avec fetch
            const response = await fetch(`http://localhost:4000/api/workingtimes/${userData.id}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ working_time }),
            });

            if (response.ok) {
                const responseData = await response.json();
                console.log('Working time created:', responseData);
            } else {
                console.error('Error creating working time:', response.status);
            }
        } catch (error) {
            console.error('Error creating working time:', error);
        }
    };

    const updateWorkingTime = async (working_time) => {
        try {
            const response = await fetch(`http://localhost:4000/api/workingtimes/${workingTimeData.id}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ working_time }),
            });

            if (response.ok) {
                console.log('Working time updated successfully');
                setworkingTimeData("");
            } else {
                console.error('Error updating working time:', response.status);
            }
        } catch (error) {
            console.error('Error updating working time:', error);
        }
    };

    const deleteWorkingTime = async () => {
        try {
            const response = await fetch(`http://localhost:4000/api/workingtimes/${workingTimeData.id}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            if (response.ok) {
                console.log('Working time deleted successfully');
                setWorkingTime(null);
                setworkingTimeData("");
            } else {
                console.error('Error deleting working time:', response.status);
            }
        } catch (error) {
            console.error('Error deleting working time:', error);
        }
    };

    return (
        <div className={"workingTimes"}>

            <div className="button-container">
                <Button variant="contained" onClick={fetchData}>Refresh</Button>

                {workingTime && (
                    <Button variant="contained" color="error" onClick={deleteWorkingTime}>Delete</Button>
                )}
            </div>
            {
                workingTime
            ?
                <SingleWorkingTime start={workingTime.start} end={workingTime.end} />
            :
                null
            }


            <div className="working-time-container">
                {/* Titre centré au-dessus */}
                <h1 className="title">Create working time</h1>

                <div className="rows">
                    {/* Colonne de gauche */}
                    <div className="column">
                        <label>Start date: </label>
                        <div>
                            <input type="date" value={selectedCreateStartDate.toISOString().split('T')[0]} onChange={(e) => setSelectedCreateStartDate(new Date(e.target.value))} />
                        </div>
                        <div>
                            <input type="time" value={selectedCreateStartTime} onChange={(e) => setSelectedCreateStartTime(e.target.value)} />
                        </div>
                    </div>

                    {/* Colonne de droite */}
                    <div className="column">
                        <label>End date: </label>
                        <div>
                            <input type="date" value={selectedCreateEndDate.toISOString().split('T')[0]} onChange={(e) => setSelectedCreateEndDate(new Date(e.target.value))} />
                        </div>
                        <div>
                            <input type="time" value={selectedCreateEndTime} onChange={(e) => setSelectedCreateEndTime(e.target.value)} />
                        </div>
                    </div>
                </div>

                <div className="centered-button">
                    <Button variant="contained" onClick={handleCreateWorkingTime}>Create working time</Button>
                </div>
            </div>

            <div className="working-time-container">
                {/* Titre centré au-dessus */}
                <h1 className="title">Edit working time</h1>

                <div className="rows">
                    {/* Colonne de gauche */}
                    <div className="column">
                        <label>Start date: </label>
                        <div>
                            <input type="date" value={selectedEditStartDate.toISOString().split('T')[0]} onChange={(e) => setSelectedEditStartDate(new Date(e.target.value))} />
                        </div>
                        <div>
                            <input type="time" value={selectedEditStartTime} onChange={(e) => setSelectedEditStartTime(e.target.value)} />
                        </div>
                    </div>

                    {/* Colonne de droite */}
                    <div className="column">
                        <label>End date: </label>
                        <div>
                            <input type="date" value={selectedEditEndDate.toISOString().split('T')[0]} onChange={(e) => setSelectedEditEndDate(new Date(e.target.value))} />
                        </div>
                        <div>
                            <input type="time" value={selectedEditEndTime} onChange={(e) => setSelectedEditEndTime(e.target.value)} />
                        </div>
                    </div>
                </div>

                {/* Bouton centré en dessous */}
                <div className="centered-button">
                    <Button variant="contained" onClick={handleEditWorkingTime}>Edit current working time</Button>
                </div>
            </div>
        </div>
    );
};

export default WorkingTimes;