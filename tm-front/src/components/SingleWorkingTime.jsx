import React from 'react';
import '../styles/SingleWorkingTime.css';

const SingleWorkingTime = ({ start, end }) => {
    const startDate = new Date(start);
    const endDate = new Date(end);

    const startTimeInMinutes = startDate.getHours() * 60 + startDate.getMinutes();
    const endTimeInMinutes = endDate.getHours() * 60 + endDate.getMinutes();
    const totalMinutes = 24 * 60;

    const leftPosition = (startTimeInMinutes / totalMinutes) * 100;
    const barWidth = ((endTimeInMinutes - startTimeInMinutes) / totalMinutes) * 100;

    return (
        <div className="single-working-time-container">
            <div className="date-info">
                {startDate.toLocaleDateString()}
            </div>
            <div className="bar-container">
                <div
                    className="bar"
                    style={{
                        left: `${leftPosition}%`,
                        width: `${barWidth}%`,
                    }}
                ></div>
                <div className="start-time" style={{ left: `${leftPosition}%` }}>
                    {startDate.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })}
                </div>
                <div className="end-time" style={{ left: `${leftPosition + barWidth}%` }}>
                    {endDate.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })}
                </div>
            </div>
        </div>
    );
};

export default SingleWorkingTime;
