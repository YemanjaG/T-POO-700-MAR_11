import React from 'react';
import '../styles/index.css';
import WorkingTimes from "./WorkingTimes.jsx";
import Clock from "./Clock.jsx";
import '../styles/WorkingTimesClocks.css';

const WorkingTimesClocks = () => {

    const [workingTimeOrClock, setWorkingTimeOrClock] = React.useState(true);
    return (
        <div className={"div3 workingTimesClocks"}>
            <div className={"workingTimesClocks-button-group"} style={{display: "flex", gap: "10px"}}>
                <button className={workingTimeOrClock ? "workingTimesClocks-button workingTimesClocks-button-active" : "workingTimesClocks-button"} onClick={()=> setWorkingTimeOrClock(!workingTimeOrClock)}>Planning</button>
                <button className={!workingTimeOrClock ? "workingTimesClocks-button workingTimesClocks-button-active" : "workingTimesClocks-button"} onClick={()=> setWorkingTimeOrClock(!workingTimeOrClock)}>Présence</button>
            </div>

            {
                workingTimeOrClock ? <WorkingTimes/> : <Clock/>
            }
        </div>
    );
};

export default WorkingTimesClocks;