import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
} from 'recharts';
import PropTypes from 'prop-types';

const SimpleLineChart = ({objectData}) => {
    // 650
    const width = innerWidth/3;
    // 400
    const height = innerHeight/3;
    return (
        <LineChart
            width={width}
            height={height}
            data={objectData}
            margin={{
                top: 5,
                right: 30,
                left: 20,
                bottom: 5
            }}
        >
            <CartesianGrid strokeDasharray="3 3"/>
            <XAxis dataKey="name"/>
            <YAxis
                type="category"
                domain={['07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00']}
            />
            <Tooltip/>
            <Legend/>
            <Line type="monotone" dataKey="start" stroke="#8884d8"
                  activeDot={{r: 8}}/>
            <Line type="monotone" dataKey="end" stroke="#82ca9d"/>
        </LineChart>
    )
}

SimpleLineChart.propTypes = {
    objectData: PropTypes.object.isRequired,
}

export default SimpleLineChart;