import React, { useEffect, useState } from 'react';
import { PieChart, Pie, Tooltip, Legend } from 'recharts';

const SimplePieChart = ({ objectData, currentTime }) => {
    const [userStatus, setUserStatus] = useState({});

    useEffect(() => {
        const sortedData = objectData.data.sort((a, b) =>
            a.user === b.user ? new Date(b.time) - new Date(a.time) : a.user - b.user
        );

        const lastRecords = Object.values(
            sortedData.reduce((acc, item) => {
                if (!acc[item.user]) {
                    acc[item.user] = item;
                }
                return acc;
            }, {})
        );

        const statusMap = lastRecords.reduce((acc, item) => {
            acc[item.user] = item.status;
            return acc;
        }, {});

        setUserStatus(statusMap);
    }, [objectData]);

    const workingCount = Object.values(userStatus).filter((status) => status === true).length;
    const notWorkingCount = Object.values(userStatus).length - workingCount;

    const pieChartData = [
        { name: 'Working', value: workingCount, fill: 'var(--primary-color)' },
        { name: 'Not Working', value: notWorkingCount, fill: 'var(--quinary-color)' },
    ];

    // Largeur et hauteur du pie chart
    const width = window.innerWidth / 3;
    const height = window.innerHeight / 3;

    return (
        <PieChart width={width} height={height}>
            <Pie
                dataKey="value"
                isAnimationActive={true}
                data={pieChartData}
                cx={width / 2}
                cy={height / 2.5}
                outerRadius={Math.min(width, height) / 3}
            />
            <Tooltip />
            <Legend />
        </PieChart>
    );
};

export default SimplePieChart;
