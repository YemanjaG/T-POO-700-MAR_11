import {
    Box,
    Typography,
    InputLabel,
    MenuItem,
    FormControl,
    Select
} from '@mui/material';
import PropTypes from 'prop-types';
import SimpleLineChart from './SimpleLineChart.jsx';
import SimpleBarChart from './SimpleBarChart.jsx';
import dayjs from 'dayjs';
import {useState} from 'react';
import SimplePieChart from "./SimplePieChart.jsx";

const ChartManager = ({data, dataType}) => {
    const [type, setType] = useState("line")
    const types = ['line', "bloc", "pie"];

    // Pansement, trouver meilleur solution
    // C'est pour éviter qu'on essaye de map sur un objet null (=crash)
    if (data == null){
        return
        return null;
    }

    const mockData = {
        "data": [
            {
                "id": 1,
                "status": true,
                "time": "2023-10-25T09:30:00",
                "user": 1
            },
            {
                "id": 2,
                "status": false,
                "time": "2023-10-25T17:30:00",
                "user": 1
            },
            {
                "id": 3,
                "status": true,
                "time": "2023-10-25T14:30:00",
                "user": 2
            },
        ]
    }

    const objectData = data.map((item) => {
        if (dataType === "WORKING_TIME"){
            const startDate = new Date(item.start);
            const endDate = new Date(item.end);
            const dateDay = dayjs(startDate).format("DD/MM/YY");
            const startHour = dayjs(startDate).format("HH:mm");
            const endHour = dayjs(endDate).format("HH:mm");

            return {
                name: dateDay,
                start: startHour,
                end: endHour
            }
        }
        else if (dataType === "CLOCK"){
            console.log("debug : " + JSON.stringify(item));
            return {
                id: item.id,
                status: item.status,
                time: item.time,
                user: item.user
            }
        }
    })

    const handleTypeChange = (event) => {
        setType(event.target.value);
    }

    let component;
    switch (type) {
        case "line":
            component = <SimpleLineChart objectData={objectData}/>;
            break;
        case "bloc":
            component = <SimpleBarChart objectData={objectData}/>;
            break;
        case "pie":
            component = <SimplePieChart objectData={{data: objectData}}/>;
            break;
        default:
            return null;
    }

    return (
        <Box width="100%">
            <Typography>Charts</Typography>
            <Box mt={2} width="100%" display="flex">
                {component}
                <FormControl>
                    <InputLabel id="type-label">Type</InputLabel>
                    <Select
                        labelId="type-label"
                        id="type-select"
                        value={type}
                        label="Label"
                        onChange={handleTypeChange}
                    >
                        {types.map(
                            (item) => <MenuItem key={item} value={item}>{item}</MenuItem>)}
                    </Select>
                </FormControl>
            </Box>
        </Box>
    )
}

ChartManager.propTypes = {
    data: PropTypes.array.isRequired,
}

export default ChartManager;