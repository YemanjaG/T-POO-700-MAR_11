import React from 'react';
import '../styles/index.css';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import '../styles/Clock.css'

const Clock = () => {

    const [isWork, setIsWork] = React.useState(false);
    const [startDateTime, setstartDateTime] = React.useState("00:00:00");

    return (
        <div className={"clockSection"}>
            <div className={"clockSectionStatus"}>

                {
                    isWork ?
                        <>
                            <CheckCircleIcon sx={{color: "green", fontSize: "25px"}}/>
                            <p>Présent depuis {startDateTime}</p>
                        </>
                    :

                    <>
                    <CheckCircleIcon sx={{color: "red", fontSize: "25px"}}/>
                     <p>Vous ne travaillez pas</p>
                    </>
                }


            </div>
        </div>
    );
};

export default Clock;