import React, {useContext, useEffect, useState} from 'react';
import {Box} from '@mui/material';
import ChartManager from './Chart/ChartManager.jsx';
import {UserContext} from '../context/UserContext.jsx';

const Charts = () => {
    const {userData, setUserData} = useContext(UserContext);
    const [data, setData] = useState(null);
    const [clockData, setClockData] = useState(null);
    const fetchWorkingTimesUser = async () => {
        const response = await fetch(
            `http://localhost:4000/api/workingtimes?user=${userData.id}`);
        const workingTimes = await response.json();
        setData(workingTimes.data);
    }

    const fetchAllClocks = async () => {
        const response = await fetch(
            `http://localhost:4000/api/clocks`);
        const clocks = await response.json();
        setClockData(clocks.data);
        console.log(clocks.data);
    }

    useEffect(() => {
        fetchWorkingTimesUser();
        fetchAllClocks();
    }, [userData.id]);

    return (
        <Box className="div4">
            {data !== null ?
                <div className="divChart">
                    <ChartManager data={data} dataType={"WORKING_TIME"}/>
                    <ChartManager data = {clockData} dataType={"CLOCK"}/>
                </div>
                : null
            }
        </Box>
    );
};

export default Charts;