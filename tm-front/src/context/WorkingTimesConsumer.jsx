import React, {createContext, PropsWithChildren, useState} from "react";

export const WorkingTimeContext = createContext(null);

export const WorkingTimeConsumer = ({ children }) => {

    const [workingTimeData, setworkingTimeData] = useState("ca marche");

    return (
        <WorkingTimeContext.Provider value={{workingTimeData, setworkingTimeData}}>
            {children}
        </WorkingTimeContext.Provider>
    );
}