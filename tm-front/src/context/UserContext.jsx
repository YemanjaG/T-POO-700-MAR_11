import React, {createContext, PropsWithChildren, useState} from "react";

export const UserContext = createContext(null);

export const UserConsumer = ({ children }) => {

    const [userData, setUserData] = useState({
        id: 1,
        username: 'John Doe',
        email: 'john@gmail.com',
    });

    const [userWorks, setUserWorks] = useState(true)
    const [userStartWorkTime, setUserStartWorkTime] = useState("03-10 08:00")

    return (
        <UserContext.Provider value={{userData, setUserData, userWorks, userStartWorkTime}}>
            {children}
        </UserContext.Provider>
    );
}