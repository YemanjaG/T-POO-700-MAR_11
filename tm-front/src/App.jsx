import {BrowserRouter, Route, Routes} from "react-router-dom";
import Dashboard from "./pages/Dashboard.jsx";
import ConnectionPage from "./pages/ConnectionPage.jsx";
import './styles/index.css';
import {UserConsumer} from "./context/UserContext.jsx";
import {WorkingTimeConsumer} from "./context/WorkingTimesConsumer.jsx";

function App() {

  return (
    <>
        <BrowserRouter>
            <UserConsumer>
                <WorkingTimeConsumer>
                    <Routes>
                        <Route path="/" element={<Dashboard />} />
                        <Route path="/connection" element={<ConnectionPage />} />
                    </Routes>
                </WorkingTimeConsumer>
            </UserConsumer>
        </BrowserRouter>

    </>
  )
}

export default App
