import '../styles/Dashboard.css';
import Navbar from "../components/Navbar.jsx";
import WorkingTimesClocks from "../components/WorkingTimesClocks.jsx";
import WorkingTime from "../components/WorkingTime.jsx";
import User from "../components/User.jsx";
import Charts from "../components/Charts.jsx";
import {UserConsumer} from "../context/UserContext.jsx";

const Dashboard = () => {
    return (
        <div className={"dashboard"}>
            <Navbar/>
            <div className="parent">
                <User/>
                <WorkingTime/>
                <WorkingTimesClocks/>
                <Charts/>
            </div>
        </div>
    );
};

export default Dashboard;