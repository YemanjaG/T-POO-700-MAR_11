# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Hello.Repo.insert!(%Hello.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
import Timex

alias Hello.Repo
alias Hello.Users.User
alias Hello.Clocks.Clock


num_users = 20


Enum.each(1..num_users, fn x ->
  username = "User#{x}"
  email = "#{username}@mail.com"

  Repo.insert!(%User{
    username: username,
    email: email,
  })


end)





