defmodule HelloWeb.WorkingTimeController do
  use HelloWeb, :controller

  alias Hello.WorkingTimes
  alias Hello.WorkingTimes.WorkingTime

  action_fallback HelloWeb.FallbackController


  def index(conn, params) do
    working_times =
      if Map.has_key?(params, "start") and Map.has_key?(params, "end") and Map.has_key?(params, "user") do
        WorkingTimes.list_working_times(user: params["user"], start: params["start"], end: params["end"])
      else
        WorkingTimes.list_working_times_user(user: params["user"])
      end

    render(conn, :index, workingtimes: working_times)
  end


  def create(conn, params) do
    # Extraire les paramètres de l'heure de travail
    working_time_params = Map.get(params, "working_time", %{})

    # Extraire l'ID de l'utilisateur
    user_id = Map.get(params, "user") |> String.to_integer()

    # Créer l'objet final
    final_object = %{
      start: Map.get(working_time_params, "start"),
      end: Map.get(working_time_params, "end"),
      user: user_id
    }

    with {:ok, %WorkingTime{} = working_time} <- WorkingTimes.create_working_time(final_object) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", ~p"/api/workingtimes/#{working_time}")
      |> render(:show, working_time: working_time)
    end
  end

  def show(conn, %{"id" => id}) do
    working_time = WorkingTimes.get_working_time!(id)
    render(conn, :show, working_time: working_time)
  end

  def update(conn, %{"id" => id, "working_time" => working_time_params}) do
    working_time = WorkingTimes.get_working_time!(id)

    with {:ok, %WorkingTime{} = working_time} <- WorkingTimes.update_working_time(working_time, working_time_params) do
      render(conn, :show, working_time: working_time)
    end
  end

  def delete(conn, %{"id" => id}) do
    working_time = WorkingTimes.get_working_time!(id)

    with {:ok, %WorkingTime{}} <- WorkingTimes.delete_working_time(working_time) do
      send_resp(conn, :no_content, "")
    end
  end
end
