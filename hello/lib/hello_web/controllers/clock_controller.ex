defmodule HelloWeb.ClockController do
  use HelloWeb, :controller

  alias Hello.Clocks
  alias Hello.Clocks.Clock

  action_fallback HelloWeb.FallbackController

#  def index(conn, %{"user" => user_id}) do
#    clocks = Clocks.list_clocks(String.to_integer(user_id))
#    render(conn, :index, clocks: clocks)
#  end

#  def index(conn, params) do
#    clocks =
#      if Map.has_key?(params, "user") do
#        Clocks.list_clocks_with_user(String.to_integer(user: params["user"]))
#      else
#        Clocks.list_clocks()
#      end
#    render(conn, :index, clocks: clocks)
#  end

  def index(conn, %{"user" => user_id} = params) do
    clocks = Clocks.list_clocks_with_user(String.to_integer(user_id))
    render(conn, :index, clocks: clocks)
  end

  def index(conn, %{}) do
    clocks = Clocks.list_clocks()
    render(conn, :index, clocks: clocks)
  end



  def create(conn, params) do

    # Extraire les paramètres de l'horloge
    clock_params = Map.get(params, "clock", %{})

    # Extraire l'ID de l'utilisateur
    user_id = Map.get(params, "user") |> String.to_integer()

    # Créer l'objet final
    final_object = %{
      status: Map.get(clock_params, "status"),
      time: Map.get(clock_params, "time"),
      user: user_id
    }

    IO.inspect(clock_params, label: "Clock Parameters")

    with {:ok, %Clock{} = clock} <- Clocks.create_clock(final_object) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", ~p"/api/clocks/#{clock}")
      |> render(:show, clock: clock)
    end
  end

#  def create(conn, %{"clock" => clock_params}) do
#    # 1. Extraire l'ID de l'utilisateur depuis l'URL
#    user_id = conn.params["id"] |> String.to_integer()
#
#    # 2. Parse le corps de la requête
#    time = clock_params["time"]
#    status = clock_params["status"] |> String.to_boolean()
#
#    # 3. Ajouter les données à la base de données
#    changeset = Hello.Clocks.Clock.changeset(%Hello.Clocks.Clock{time: time, status: status, user: user_id})
#
#    case Repo.insert(changeset) do
#      {:ok, _clock} ->
#        conn
#        |> put_status(:created)
#        |> render("show.json", clock: _clock)
#      {:error, changeset} ->
#        conn
#        |> put_status(:unprocessable_entity)
#        |> render(HelloWeb.ChangesetView, "error.json", changeset: changeset)
#    end
#  end


  def show(conn, %{"id" => id}) do
    clock = Clocks.get_clock!(id)
    render(conn, :show, clock: clock)
  end

  def update(conn, %{"id" => id, "clock" => clock_params}) do
    clock = Clocks.get_clock!(id)

    with {:ok, %Clock{} = clock} <- Clocks.update_clock(clock, clock_params) do
      render(conn, :show, clock: clock)
    end
  end

  def delete(conn, %{"id" => id}) do
    clock = Clocks.get_clock!(id)

    with {:ok, %Clock{}} <- Clocks.delete_clock(clock) do
      send_resp(conn, :no_content, "")
    end
  end
end
