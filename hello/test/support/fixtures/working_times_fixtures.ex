defmodule Hello.WorkingTimesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Hello.WorkingTimesClocks` context.
  """

  @doc """
  Generate a working_time.
  """
  def working_time_fixture(attrs \\ %{}) do
    {:ok, working_time} =
      attrs
      |> Enum.into(%{
        end: ~N[2023-10-23 08:49:00],
        start: ~N[2023-10-23 08:49:00],
        user: "some user"
      })
      |> Hello.WorkingTimes.create_working_time()

    working_time
  end

  @doc """
  Generate a working_time.
  """
  def working_time_fixture(attrs \\ %{}) do
    {:ok, working_time} =
      attrs
      |> Enum.into(%{
        end: ~N[2023-10-23 08:53:00],
        start: ~N[2023-10-23 08:53:00]
      })
      |> Hello.WorkingTimes.create_working_time()

    working_time
  end
end
