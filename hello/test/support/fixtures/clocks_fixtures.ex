defmodule Hello.ClocksFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Hello.Clocks` context.
  """

  @doc """
  Generate a clock.
  """
  def clock_fixture(attrs \\ %{}) do
    {:ok, clock} =
      attrs
      |> Enum.into(%{
        status: true,
        time: ~N[2023-10-23 08:47:00]
      })
      |> Hello.Clocks.create_clock()

    clock
  end

  @doc """
  Generate a clock.
  """
  def clock_fixture(attrs \\ %{}) do
    {:ok, clock} =
      attrs
      |> Enum.into(%{
        status: true,
        time: ~N[2023-10-23 08:54:00]
      })
      |> Hello.Clocks.create_clock()

    clock
  end
end
